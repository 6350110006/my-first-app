import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirstApp());
}

class MyFirstApp extends StatelessWidget {
  const MyFirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PSU Trang',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.ac_unit_sharp),
          title: Text('My first App'),
          actions: [
            IconButton(
                onPressed:(){},
                icon: Icon(Icons.abc_outlined),
            ),
            IconButton(
              onPressed:(){},
              icon: Icon(Icons.account_box),
            ),
          ],
        ),
        body: Center(
          child: Column(
            children: [
              // Image.asset('assets/nasma.jpg',
              //   height: 300,
              //   width: 300,
              // ),

              CircleAvatar(
                backgroundImage: AssetImage('assets/nasma.jpg',),
                radius: 100,
              ),
              Text("Nasma Supawan ",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        )
      ),
    );
  }
}

// class MyFirstApp extends StatelessWidget {
//   const MyFirstApp({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home:
//     );
//   }
// }


